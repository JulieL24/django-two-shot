* [x] Fork and clone the starter project from django-one-shot 
* [x] Create a new virtual environment in the repository directory for the project
* [x] Activate the virtual environment
  * [x] python -m venv .venv 
  * [x] source .venv/bin/activate
* [x] Upgrade pip
  * [x] python -m pip install --upgrade pip
* [x] Install django
  * [x] pip install django  
* [x] Install black
  * [x] pip install black
* [x] Install flake8
  * [x] pip install flake8
* [x] Install djhtml 
  * [x] pip install djhtml  
* [x] Install djlint
  * [x] pip install djlint
  * [x] install debug toolbar
    * [x] pip install django-debug-toolbar  
* [x] Deactivate your virtual environment
  * [x] deactivate 
* [x] Activate your virtual environment
  * [x] source .venv/bin/activate
* [x] Use pip freeze to generate a requirements.txt file
  * [x] pip freeze > requirements.txt 
 * [x] create django project expenses
    * [x] django-admin startproject expenses .
* [x] * run migrations
  * [x] python manage.py migrate  
* [ ] create superuser 
  * [ ] python manage.py createsuperuser 
  * [x] create 2 apps accounts and receipts 
    * [x] python manage.py startapp <<name>>
* [x] add app into settings.py installed app
  * [x] "<<name>>.apps.<<UppercaseName>>Config"
* [x] In the receipts django app create 3 models
  * [x] ExpenseCategory, Receipts, Account
  * [x] ExpenseCategory model
    * [x] name, owner, __str__ returns name value
  * [x] Account model  
    * [x] name, number, owner, __str__ returns name value
  * [x] Receipt model 
    * [x] vendor, total, tax, date, purchaser, category, account
  * [x] register models in admin  
  * [x] create list view for receipt model  
    * [x] register url in urls in app
    * [x] register url in urls in project
    * [x] create receipt list template
    * [x] create login view in accounts app
    * [x] update urls and create template
      * [x] link urls to the expense project  
    * [x] create logout redirecting to home page 
    * [x] create function view to handle sign-up form and handle its submission
    * [x] create login and logout button in the header
    * [x] create new receipts form
    * [x] create list view for account and expense category model 
    * [x] create a new expense category model 
    * [x] create link in home page to link to each create page



## resources 
<https://learn-2.galvanize.com/cohorts/3352/blocks/1859/content_files/build/02-django-one-shot/65-django-one-shot-00.md>

<https://learn-2.galvanize.com/cohorts/3352/blocks/1859/content_files/build/03-django-two-shot/64-django-two-shot-reference-guide.md>

<https://docs.djangoproject.com/en/4.0/ref/models/fields/>

<https://docs.djangoproject.com/en/4.0/topics/auth/default/#all-authentication-views-1>