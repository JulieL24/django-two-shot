from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView
from receipts.models import Receipt, Account, ExpenseCategory
from django.shortcuts import redirect

# Create your views here.


class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    context_object_name = "receiptlist"
    template_name = "receipts/receipt_list.html"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    fields = ["vendor", "total", "tax", "date", "category", "account"]
    template_name = "receipts/create_receipt.html"

    def form_valid(self, form):
        item = form.save(commit=False)
        item.purchaser = self.request.user
        item.save()
        return redirect("home")


class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    template_name = "accounts/accounts_list.html"

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)


class ExpenseCategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = "expense/expense_list.html"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)


class ExpenseCategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    fields = ["name"]
    template_name = "expense/create_category.html"

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("expense_category_list")


class AccountCreateView(CreateView):
    model = Account
    fields = [
        "name",
        "number",
    ]
    template_name = "accounts/create_account.html"

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("account_list")
