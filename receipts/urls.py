from django.urls import path

from receipts.views import (
    ExpenseCategoryListView,
    ReceiptListView,
    ReceiptCreateView,
    AccountListView,
    ExpenseCategoryCreateView,
    AccountCreateView,
)

urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("create/", ReceiptCreateView.as_view(), name="create_receipt"),
    path("accounts/", AccountListView.as_view(), name="account_list"),
    path(
        "categories/",
        ExpenseCategoryListView.as_view(),
        name="expense_category_list",
    ),
    path(
        "categories/create/",
        ExpenseCategoryCreateView.as_view(),
        name="create_category",
    ),
    path(
        "accounts/create/", AccountCreateView.as_view(), name="create_account"
    ),
]
